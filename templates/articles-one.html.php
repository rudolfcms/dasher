<?php include '_head.html.php'; ?>

<main class="main">
	<h1><?=$this->pageTitle();?></h1>

	<?=$this->alerts(); ?>

	<form action="<?=$this->path;?>" method="post">
		<div class="form-group">
			<label class="form-label" for="title"><?=_('title');?></label>
			<input type="text" name="title" id="title" class="input" placeholder="<?=_('title');?>" value="<?=$this->article->title('raw');?>"/>
		</div>
		<div class="form-group">
			<textarea class="textarea" name="content"><?=$this->article->content(false, false, true);?></textarea>
		</div>

		<div class="form-group">
			<label class="form-label" for="slug"><?=_('slug');?></label>
			<input type="text" name="slug" id="slug" class="input" placeholder="<?=_('slug');?>" value="<?=$this->article->slug();?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="keywords"><?=_('keywords');?></label>
			<input type="text" name="keywords" id="keywords" class="input" placeholder="<?=_('keywords');?>" value="<?=$this->article->keywords();?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="description"><?=_('description');?></label>
			<input type="text" name="description" id="description" class="input" placeholder="<?=_('description');?>" value="<?=$this->article->description();?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="date"><?=_('date');?></label>
			<input type="text" name="date" id="date" class="input" placeholder="<?=_('date');?>" value="<?=$this->article->date();?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="author"><?=_('author');?></label>
			<input type="text" name="author" id="author" class="input" placeholder="<?=_('author');?>" value="<?=$this->article->author(false);?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="album"><?=_('album');?></label>
			<input type="text" name="album" id="album" class="input" placeholder="<?=_('album');?>" value="<?=$this->article->album();?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="thumb"><?=_('thumb');?></label>
			<input type="text" name="thumb" id="thumb" class="input" placeholder="<?=_('thumb');?>" value="<?=$this->article->thumb();?>"/>
		</div>
		<div class="form-group">
			<label class="form-label" for="photos"><?=_('photos');?></label>
			<input type="text" name="photos" id="photos" class="input" placeholder="<?=_('photos');?>" value="<?=$this->article->photos();?>"/>
		</div>

		<div class="form-group">
			<?php if('edit' === $this->templateType): ?><a href="<?=$this->article->delUrl();?>" class="btn text-red">Usuń</a>
			<input name="update" type="submit" class="submit" value="Aktualizuj"/><?php else: ?>
			<input name="add" type="submit" class="submit" value="Dodaj"/><?php endif; ?>
		</div>
	</form>

</main>

<?php include '_foot.html.php' ;?>