<?php include '_head.html.php'; ?>

<main class="main">
	<h1>profile</h1>

	
	<ul>
		<li><i class="fa fa-user"></i> name: <b><?=$this->getUserFullName();?></b> 
			/ <b><?=$this->getUserNick();?></b></li>
		<li><i class="fa fa-envelope-o"></i> email: <b><?=$this->getUserEmail();?></b></li>
		<li><i class="fa fa-calendar"></i> register date: <b><?=$this->getUserRegisterDate();?></b></li>
	</ul>
</main>

<?php include '_foot.html.php' ;?>