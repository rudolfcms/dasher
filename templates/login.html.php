<!doctype html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="<?=$this->themePath;?>/css/login.css"/>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	</head>
	<body>
		<div class="c">
			<h1>Log in</h1>
			<?php if($this->isMessage()):?> 
			<div class="alert alert-<?=$this->getMessage('type');?>"><?=$this->getMessage('message');?></div>
			<?php endif;?> 
			<form action="" method="post" class="form">
				<div class="form-group">
					<i class="fa fa-user"></i>
					<input name="email" value="<?=$this->getNick();?>" type="text" autofocus="" class="input" placeholder="email or nick" required=""/>
				</div>
				<div class="form-group">
					<i class="fa fa-lock"></i>
					<input name="password" value="" type="password" class="input" placeholder="password" required=""/>
				</div>
				<div class="form-group">
					<input class="pull-right submit" type="submit" name="send" value="Login"/>
				</div>
			</form>
			<a href="<?=DIR;?>">Return to site</a>
		</div>
	</body>
</html>