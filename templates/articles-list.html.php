<?php include '_head.html.php'; ?>

<main class="main">
	<h1><?=$this->pageTitle();?></h1>

<?php if($this->loop->isItems()): ?>
<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>title</th>
			<th>added</th>
			<th>author</th>
			<th>action</th>
		</tr>
	</thead>
	<tbody>
<?php while($this->loop->haveItems()): $a = $this->loop->item(); ?> 
		<tr>
			<th><?=$a->id();?></th>
			<td><?=$a->title();?></td>
			<td><?=$a->date();?></td>
			<td><?=$a->author();?></td>
			<td><a href="<?=$a->editUrl();?>">Edytuj</a> | <a href="<?=$a->delUrl();?>">Usuń</a></td>
		</tr>
<?php endwhile;?> 
	</tbody>
</table>

<?php if ($this->loop->isPagination()): ?> 
	<nav role="navigation" class="pagination-container">
		<?=$this->loop->nav(['ul'=>'pagination', 'current'=>'current'], 2);?>
	</nav>
<?php endif;?> 

<?php else: ?> 
	<div class="alert info">Brak artykułów do wyświetlenia!</div>
<?php endif;?> 

</main>

<?php include '_foot.html.php' ;?>