<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<title><?php $this->head->title();?></title>
	<link rel="stylesheet" href="<?=$this->themePath;?>/css/dashboard.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro&amp;subset=latin,latin-ext' rel='stylesheet'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
	<div class="wrapper">
		<header class="header">
			<span><a href="<?=DIR;?>/admin">dashboard</a></span>

			<nav class="top-menu-container">
				<?=$this->pageNav('top-right', $nesting = 4, 
					['li_active' => 'active', 'root_ul' => 'top-menu pull-right']
				); ?>
			</nav>
		</header>
		
		<aside class="menu-sidebar">
			<nav class="main-menu">
				<?=$this->pageNav('main', $nesting = 4, 
					['li_active' => 'active']
				); ?>
				<ul>
					<li class="divider"></li>
				</ul>
				<?=$this->pageNav('tools', $nesting = 4, 
					['li_active' => 'active']
				); ?> 
			</nav>
		</aside>
